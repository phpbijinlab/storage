<?php

namespace BijinLab\Component\Storage\Queue\Aws\Sqs;

use Aws\Sqs\SqsClient;

/**
 * Sqs class implements
 * @author ushio
 *
 */
abstract class AbstractSqsQueue implements SqsQueueInterface
{
    
    /**
     * sqs object.
     * @var unknown
     */
    protected $sqs;
    
    /**
     * Sqs arn url.
     * @var unknown
     */
    protected $url;
    
    /**
     * Set sqs object
     * @param unknown $sqs
     */
    public function setSqs(SqsClient $sqs)
    {
        $this->sqs = $sqs;
    }
    
    /**
     * set sqs arn url.
     * @param unknown $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
    
    /**
     * @{inheritdoc}
     */
    public function fetch($options = array())
    {
        $results = $this->sqs->receiveMessage(array_merge(array(
                'QueueUrl' => $this->url
        ), $options));
        
        if (is_null($results['Messages'])) return null;
        
        foreach($results->getPath('Messages/*/Body') as $messageBody){
            $body = $messageBody;
        }
        
        foreach($results->getPath('Messages/*/ReceiptHandle') as $handle){
            $this->delete($handle);
        }
        
        return $body;
    }
    
    /**
     * {@inheritdoc}
     */
    public function save($data, $options = array())
    {
        $result = $this->sqs->sendMessage(array_merge(array(
                'QueueUrl' => $this->url,
                'MessageBody' => $data
        ), $options));
        
        return $result;
    }
    
    /**
     * @{inheritdoc}
     */
    public function delete($receiptHandle = null, $options = array())
    {
        if (!$receiptHandle){
            $fetchResult = $this->fetch();
            foreach($fetchResult->getPath('Messages/*/ReceiptHandle') as $handle){
                $receiptHandle = $handle;
            }
        }
        
        if (isset($receiptHandle) && (is_null($receiptHandle) == false) ){
            $result = $this->sqs->deleteMessage(array_merge(array(
                    'QueueUrl' => $this->url,
                    'ReceiptHandle' => $receiptHandle
            ), $options));
        }else{
            return false;
        }
        
        return $result;
    }
}