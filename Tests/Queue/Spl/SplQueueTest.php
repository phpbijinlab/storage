<?php

namespace BijinLab\Component\Storage\Tests\Queue\Spl;

use BijinLab\Component\Storage\Queue\Spl\SplQueue as StorageSplQueue;

/**
 * Spl queue class tests.
 * @author ushio
 *
 */
class SplQueueTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateInstance()
    {
        $queue = new StorageSplQueue();
        
        $this->assertFalse(is_null($queue));
    }
    
    public function testEnqueueAndDequeue()
    {
        $splQueue = new \SplQueue();
        
        $queue = new StorageSplQueue();
        $queue->setSplQueue($splQueue);
        
        $queue->save('hogehoge');
        $queue->save('hogahoga');
        $queue->save('aaaaaaa');

        $this->assertEquals('hogehoge', $queue->fetch());
        $this->assertEquals('hogahoga', $queue->fetch());
    }
}