<?php
namespace BijinLab\Component\Storage\File;

use BijinLab\Component\Image\File\Image;
use BijinLab\Component\Image\File\ImageInterface;

/**
 * Storage driver interface.
 * 
 * This is interface for file save to any storage.
 * 
 * @author ushio
 *
 */
abstract class AbstractStorage implements StorageInterface
{
    
}