<?php
namespace BijinLab\Component\Storage\Tests\PhpUnit;

class StorageTestCase extends \PHPUnit_Framework_TestCase
{
    const ASSET_URI = 'http://static.bijint.com/assets/';
    public static function getSampleFilenameList()
    {
        $sampleFilenameList = array(
                '1215.png'
        );
        
        return $sampleFilenameList;
    }
    
    public static function getOutputedFilenameList()
    {
        $outputedFilenameList = array(
                'localStorage001.png'
        );
        return $outputedFilenameList;
    }
        
    public static function getAssetsUri()
    {
        return self::ASSET_URI.'/bijinlab/storage/resources';
    }
    
    public static function getSampleAssetsUrl()
    {
        return self::getAssetsUri().'/samples';
    }
    
    public static function getOutputedAssetsUrl()
    {
        return self::getAssetsUri().'/outputed';
    }
    public static function getSampleDir()
    {
        return __DIR__.'/../../Resources/samples';
    }
    
    public static function getOutputsDir()
    {
        return __DIR__.'/../../Resources/outputs';
    }
    
    public static function getOutputedDir()
    {
        return __DIR__.'/../../Resources/outputed';
    }
    
    public function setup()
    {
        return self::downloadImages();
    }
    
    public static function downloadImages()
    {
        // get samples
        $sampleFilenameList = self::getSampleFilenameList();
        
        foreach ($sampleFilenameList as $filename){
            $dir = self::getSampleDir();
            if (!file_exists($dir.'/'.$filename)){
                $body = file_get_contents(self::getSampleAssetsUrl().'/'.$filename);
                file_put_contents($dir.'/'.$filename, $body);
            }
        }
        
        // get outputed
        $outputedFilenameList = self::getOutputedFilenameList();
        
        foreach ($outputedFilenameList as $filename){
            $dir = self::getOutputedDir();
            if (!file_exists($dir.'/'.$filename)){
                $body = file_get_contents(self::getOutputedAssetsUrl().'/'.$filename);
                file_put_contents($dir.'/'.$filename, $body);
            }
        }
    }
}