<?php
namespace BijinLab\Component\Storage\Queue\Spl;

use BijinLab\Component\Storage\Queue\AbstractQueue;

/**
 * SplQueue implements.
 * @author ushio
 *
 */
abstract class AbstractSplQueue extends AbstractQueue implements SplQueueInterface
{
    /**
     * Splqueue.
     * @var unknown
     */
    protected $splQueue;
    
    /**
     * Set splqueue object.
     * @param unknown $splQueue
     */
    public function setSplQueue(\SplQueue $splQueue)
    {
        $this->splQueue = $splQueue;
    }
    
    /**
     * {@inheritdoc}
     */
    public function fetch()
    {
        if($this->splQueue->isEmpty()) return null;
        
        return $this->splQueue->dequeue();
    }
    
    /**
     * {@inheritdoc}
     */
    public function save($data)
    {
        return $this->splQueue->enqueue($data);
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        return $this->splQueue->dequeue();
    }
}