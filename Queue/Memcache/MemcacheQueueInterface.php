<?php

namespace BijinLab\Component\Storage\Queue\Memcache;

use BijinLab\Component\Storage\Queue\QueueInterface;

/**
 * Memcached interface queue servcies class interface.
 * @author ushio
 *
 */
interface MemcacheQueueInterface extends QueueInterface
{
    
}