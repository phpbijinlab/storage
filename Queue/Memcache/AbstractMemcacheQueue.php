<?php

namespace BijinLab\Component\Storage\Queue\Memcache;

use BijinLab\Component\Storage\Queue\AbstractQueue;

/**
 * Base class of queue that use Memcached interface.
 * @author ushio
 *
 */
abstract class AbstractMemcacheQueue extends AbstractQueue implements MemcacheQueueInterface
{
    
}