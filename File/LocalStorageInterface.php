<?php
namespace BijinLab\Component\Storage\File;

/**
 * Local storage driver interface.
 * 
 * This is interface for file save to local storage.
 * 
 * @author ushio
 *
 */
interface LocalStorageInterface extends StorageInterface
{
}