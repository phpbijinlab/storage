<?php
namespace BijinLab\Component\Storage\File;
/**
 * S3 driver implements.
 * 
 * This is implements for file save to S3.
 * 
 * @author ushio
 *
 */
abstract class AbstractLocalStorage extends AbstractStorage implements
        LocalStorageInterface
{

    /**
     * save root.
     * @var unknown
     */
    protected $rootDir;

    /**
     * Set root dir.
     * @param unknown $rootDir
     */
    public function setRootDir($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * Get root dir.
     * @return \BijinLab\Component\Storage\File\unknown
     */
    public function getRootDir()
    {
        if ($this->rootDir) {
            return $this->rootDir;
        } else {
            return '';
        }
    }

    /**
     * Get from local storage.
     * @param unknown $path
     */
    public function get($path)
    {
        if (file_exists($path)) {
            return $path;
        } else {
            return false;
        }
    }

    /**
     * Save to local storage.
     * @param unknown $srcPath
     * @param unknown $destPath
     * @return filepath if exist. null is failed
     */
    public function save($srcPath, $destPath)
    {
        $destPath = $this->getRootDir() . $this->getDirectorySeparator() . $destPath;
        
        $parentDir = dirname($destPath);
        if (!file_exists($parentDir)) {
            mkdir($parentDir, 0740, true);
        }

        if (file_exists($srcPath)) {
            if (file_exists($destPath)) {
                unlink($destPath);
            }

            $src = new \SplFileObject($srcPath, "r");
            $dest = new \SplFileObject($destPath, "a");
            while (!$src->eof()) {
                $dest->fwrite($src->fgets());
            }
        } else {
            return null;
        }

        return $destPath;
    }

    /**
     * {@inheritdoc}
     */
    public function isExist($path)
    {
        return file_exists($path);
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectorySeparator()
    {
        return DIRECTORY_SEPARATOR;
    }

}
