<?php

namespace BijinLab\Component\Storage\Queue\Aws\Sqs;

use BijinLab\Component\Storage\Queue\QueueInterface;

/**
 * Aws SQS interface.
 * @author ushio
 *
 */
interface SqsQueueInterface extends QueueInterface
{
    
}