<?php
namespace BijinLab\Component\Storage\Tests\Queue\Aws\Sqs;

use BijinLab\Component\Storage\Queue\Aws\Sqs\SqsQueue;

class SqsQueueTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateInstance()
    {
        $queue = new SqsQueue();
        
        $this->assertFalse(is_null($queue));
    }
}