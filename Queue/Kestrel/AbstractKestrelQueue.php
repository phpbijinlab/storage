<?php

namespace BijinLab\Component\Storage\Queue\Kestrel;

use BijinLab\Component\Storage\Queue\Memcache\AbstractMemcacheQueue;

/**
 * Kestrel queue service impelements.
 * @author ushio
 *
 */
abstract class AbstractKestrelQueue extends AbstractMemcacheQueue implements KestrelQueueInterface
{
    
    /**
     * Kestrel connection ( maybe memcached object ).
     * @var unknown
     */
    protected $kestrel;
    
    /**
     * Set kestrel connection instance.
     * @param unknown $kestrel
     */
    public function setKestrel($kestrel)
    {
        $this->kestrel = $kestrel;
    }
    
    /**
     * @{inheritdoc}
     */
    public function fetch($key=null, $option = array())
    {
        throw new \ErrorException('not implement yet.');
    }
    
    /**
     * {@inheritdoc}
     */
    public function save($data, $key=null)
    {
        throw new \ErrorException('not implement yet.');
    }
    
    /**
     * @{inheritdoc}
     */
    public function delete($key=null)
    {
        throw new \ErrorException('not implement yet.');
    }
}