<?php
namespace BijinLab\Component\Storage\File;

use Aws\S3\Enum\CannedAcl;
use Aws\S3\S3Client;

use Guzzle\Http\EntityBody;

/**
 * S3 driver implements.
 * 
 * This is implements for file save to S3.
 * 
 * @author ushio
 *
 */
abstract class AbstractS3 extends AbstractStorage implements S3Interface
{
    /**
     * s3 object
     * @var unknown
     */
    protected $s3;
    
    /**
     * S3 Bucket name.
     * @var string Bucket name.
     */
    protected $bucketName;
    
    /**
     * {@inheritdoc}
     */
    public function setS3($s3)
    {
        $this->s3 = $s3;
    }

    /**
     * {@inheritdoc}
     */
    public function setBucketName($bucketName)
    {
        $this->bucketName = $bucketName;
    }

    /**
     * Get S3 bucket name.
     * @return string
     */
    protected function getBucketName()
    {
        return $this->bucketName;
    }
    
    /**
     * Get data from s3.
     * @param unknown $key
     */
    public function get($key, $options = array())
    {
        $bucketName = $this->getBucketName();
        
        $saveAs = null;
        if (!array_key_exists('SaveAs', $options)){
            $saveAs = tempnam(sys_get_temp_dir(),'BijinLabStorageComponentS3');
            
            $pathInfo = pathinfo($key);
            if (array_key_exists('extension', $pathInfo)){
                $saveAs = $saveAs . '.' . $pathInfo['extension'];
            }
            
            $options['SaveAs'] = $saveAs;
        }
        
        $result = $this->s3->getObject(array_merge(array(
                'Bucket' => $bucketName,
                'Key' => $key
        ), $options));
        
        return $options['SaveAs'];
    }

    /**
     * Save data to s3.
     * @param unknown $srcPath
     * @param unknown $destKey
     * @param unknown $options
     * @return filepath if exist. null is failed
     */
    public function save($srcPath, $destKey, $options = array())
    {
        $bucketName = $this->getBucketName();
        $data = $this->getFileData($srcPath);
        
        if (is_null($data)) return null;
        
        $result = $this->s3->putObject(array_merge(array(
                'Bucket' => $bucketName,
                'Key' => $destKey,
                'Body' => EntityBody::factory($data)
        ), $options));
        
        return $destKey;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isExist($path, $options = array())
    {
        $bucketName = $this->getBucketName();
        
        $result = $this->s3->doesObjectExist($bucketName, $path, $options);
        
        return $result;
    }
    
    /**
     * Get file data.
     * @param unknown $srcPath
     * @return Ambigous <NULL, string>
     */
    protected function getFileData($srcPath)
    {
        $data = null;
        if (file_exists($srcPath)){
            $file = new \SplFileObject($srcPath, "r");
            while (!$file->eof()){
                $data = $data.$file->fgets();
            }
        }
        
        return $data;
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectorySeparator()
    {
        return '/';
    }
}
