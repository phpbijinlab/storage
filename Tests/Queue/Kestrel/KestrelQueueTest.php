<?php

namespace BijinLab\Component\Storage\Tests\Queue\Kestrel;

use BijinLab\Component\Storage\Queue\Kestrel\KestrelQueue;

/**
 * Kestrel queue class tests.
 * @author ushio
 *
 */
class KestrelQueueTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateInstance()
    {
        $queue = new KestrelQueue();
        
        $this->assertFalse(is_null($queue));
    }
}