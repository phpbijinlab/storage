<?php
namespace BijinLab\Component\Storage\Queue\Spl;

use BijinLab\Component\Storage\Queue\QueueInterface;

/**
 * SplQueue interface.
 * @author ushio
 *
 */
Interface SplQueueInterface extends QueueInterface
{
    
}