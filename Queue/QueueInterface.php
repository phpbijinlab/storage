<?php

namespace BijinLab\Component\Storage\Queue;

/**
 * Queueing class interface.
 * @author ushio
 *
 */
interface QueueInterface
{
    /**
     * Fetch from queue service.
     * @param unknown $key
     */
    public function fetch();
    
    /**
     * Save data to queue service.
     * @param unknown $data
     * @param unknown $key
     */
    public function save($data);
    
    /**
     * Delee data from queue service.
     * @param unknown $key
     */
    public function delete();
}