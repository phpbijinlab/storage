<?php
namespace BijinLab\Component\Storage\File;

/**
 * Storage driver interface.
 * 
 * This is interface for file save to any storage.
 * 
 * @author ushio
 *
 */
interface StorageInterface
{
    /**
     * Get from storage.
     * @param unknown $path
     */
    public function get($path);
    
    
    /**
     * Save to storage
     * @param unknown $srcPath
     * @param unknown $destPath
     * @return filepath if exist. null is failed
     */
    public function save($srcPath, $destPath);
    
    /**
     * Check exist file.
     * @param unknown $path
     * @return boolean
     */
    public function isExist($path);
    
    /**
     * Get storage system directory separtor.
     */
    public function getDirectorySeparator();
}