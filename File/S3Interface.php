<?php
namespace BijinLab\Component\Storage\File;

use \Bijint\AwsBundle\Common\Aws;

/**
 * S3 driver interface.
 * 
 * This is interface for file save to AWS S3.
 * 
 * @author ushio
 *
 */
interface S3Interface extends StorageInterface
{
    /**
     * set S3 object.
     * @param unknown $s3
     */
    public function setS3($s3);
    
    /**
     * Set S3 bucket name.
     * @param unknown $bucketName
     */
    public function setBucketName($bucketName);
}