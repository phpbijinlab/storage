<?php
namespace BijinLab\Component\Storage\Tests\File;
use BijinLab\Component\Storage\File\S3;
use BijinLab\Component\Storage\Tests\PhpUnit\StorageTestCase;

/**
 * 
 * @author ushio
 *
 */
class S3Test extends StorageTestCase
{

    public function testCreateS3Instance()
    {
        $storage = new S3();
        
        $this->assertFalse(is_null($storage));
    }
}

