<?php

namespace BijinLab\Component\Storage\Queue\Kestrel;

use BijinLab\Component\Storage\Queue\Memcache\MemcacheQueueInterface;

/**
 * Kestrel queue service class interface.
 * @author ushio
 *
 */
interface KestrelQueueInterface extends MemcacheQueueInterface
{
    
}