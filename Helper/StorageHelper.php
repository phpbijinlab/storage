<?php 
namespace BijinLab\Component\Storage\Helper;

class StorageHelper
{
    /**
     * get random string.
     * @param unknown $length
     * @return string
     */
    static public function createRandomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //$characters = '0';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[rand(0, strlen($characters)-1)];
        }
        return $randstring;
    }
}