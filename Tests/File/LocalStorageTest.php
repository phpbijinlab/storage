<?php
namespace BijinLab\Component\Storage\Tests\File;
use BijinLab\Component\Storage\File\LocalStorage;
use BijinLab\Component\Storage\Tests\PhpUnit\StorageTestCase;

/**
 * 
 * @author ushio
 *
 */
class LocalStorageTest extends StorageTestCase
{

    public function testCreateLocalStorageInstance()
    {
        $localStorage = new LocalStorage();
        
        $this->assertFalse(is_null($localStorage));
    }
    
    public function testSaveToLocalPng()
    {
        $localStorage = new LocalStorage();
        
        $path = $localStorage->get(parent::getSampleDir().'/1215.png');
        $localStorage->save($path, parent::getOutputsDir().'/localStorage001.png');
        
        $this->assertEquals(
                md5_file(parent::getOutputedDir().'/localStorage001.png'),
                md5_file(parent::getOutputsDir(). '/localStorage001.png')
        );
    }
    
    public function testIsExist()
    {
        $localStorage = new LocalStorage();
        
        $path = $localStorage->get(parent::getSampleDir().'/1215.png');
        $localStorage->save($path, parent::getOutputsDir().'/localStorage001.png');

        $this->assertTrue($localStorage->isExist(parent::getOutputsDir().'/localStorage001.png'));
        $this->assertFalse($localStorage->isExist(parent::getOutputsDir().'/localStorage001.jpg'));
    }
}

